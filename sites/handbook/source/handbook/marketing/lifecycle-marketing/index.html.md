---
layout: handbook-page-toc
title: Lifecycle Marketing
description: Lifecycle Marketing Handbook
twitter_image: /images/tweets/handbook-marketing.png
twitter_site: '@gitlab'
twitter_creator: '@gitlab'
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Lifecycle Marketing

## Overview
{: #overview .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

[Lifecycle Marketing](https://about.gitlab.com/job-families/marketing/marketing-campaign-manager/#user-lifecycle-marketing) is also a specialization within the Marketing Campaigns team. The lifecycle marketing manager is responsible for strategizing, executing, and optimizing a data-driven user lifecycle communications strategy.

This role works closely with the Product Growth and Data team, among others, to collaborate, plan, and prioritize communication to better engage GitLab users and progress them through the buyer and customer lifecycle. The lifecycle marketing manager will focus on user paths to revenue, including engaging free .com users, trial users, and paid users of GitLab.

[Read the job family here >>](https://about.gitlab.com/job-families/marketing/marketing-campaign-manager/#user-lifecycle-marketing)

## Free User Efficiency Project
{: #free-user-effficiency-project .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

Add in details

### Purpose
{: #purpose}
<!-- DO NOT CHANGE THIS ANCHOR -->
To outline and organize overall marketing campaign strategy aimed at converting free users to paid customers. 

### Team
{: #team}
<!-- DO NOT CHANGE THIS ANCHOR -->
* Lifecycle Marketing: @Aklatzkin (supported by manager, @jgragnola) 
* Growth Product: @jstava 
* Marketing Operations: @amy.waller 
* Data Team: @iweeks 

### Success Metrics
{: #metrics}
<!-- DO NOT CHANGE THIS ANCHOR -->
Free to Paid Conversion Rate 

### Key Resources
{: #key-resources}
<!-- DO NOT CHANGE THIS ANCHOR -->
* [Free User Conversion Epic](https://gitlab.com/gitlab-com-top-initiatives/free-saas-user-efficiency/free-saas/-/issues/38) - this is where weekly status updates live
* [Overall Epic for Lifecycle Marketing](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1040)
* [Epic for PQL Scenarios](https://gitlab.com/groups/gitlab-com/marketing/-/epics/2438)
* [Issue: active First Order namespaces without recent outreach from GitLab](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/2137)

## Lifecycle Campaign Development Process
{: #campaign-process .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

The Lifecycle Marketing Manger is responsible for coordinating execution of Free User campaigns. They will develop an initial strategy in collaboration with Growth Product, Marketing Ops, Data, and other teams. The steps to execute have certain issue templates, which are built into the process (see epic code below).

**Overall Steps:**
* Team defines segment / PQL audience to target
* Team collaborates/brainstorms approach
* Lifecycle Marketer opens epic
* Depending on approach decided, Lifecycle Marketer opens relevant issues (across teams)
* Lifecycle Marketer outlines timeline
* Lifecycle Marketer provides status updates to Free User Efficiency Project

#### Workback Timeline
{: #workback-timeline}
<!-- DO NOT CHANGE THIS ANCHOR -->

The workback timeline is to be created by the Lifecycle Marketer to organize the execution of a new campaign. The workback is a starting point, to be discussed and agreed with the teams involved.

[Workback Template](https://docs.google.com/spreadsheets/d/1Mrp0xNkpbfS_kvqDB-kFhhBqD-sNK86trx3qswOe6B0/edit?usp=sharing)

#### Epic Code
{: #epic-code}
<!-- DO NOT CHANGE THIS ANCHOR -->

***The Lifecycle Marketer will create the epic with the following structure. Issues will be created an related to this overarching epic to organize execution action items.***

```
<!-- NAME THIS EPIC: Free User Campaign: [Name of Campaign] (i.e. Free User Campaign: active First Order namespaces without recent outreach from GitLab) -->

### [Campaign Execution Timeline >>]() - to be created by Lifeycle Marketer <!-- Template: https://docs.google.com/spreadsheets/d/1Mrp0xNkpbfS_kvqDB-kFhhBqD-sNK86trx3qswOe6B0/edit?usp=sharing -->

### :star: Background and Scope
<!-- Provide a brief overview of the audience, campaign, and project. How did this idea arise? -->
<!-- Example: Target leads that have indicated that they are using GitLab for company use when signing up for free user license -->

### :dart: Goal
* Primary KPI: 
* Secondary KPI: 
* Measurement Plan (to be developed with Business Analytics)
   - For reference: [UTM Strategy Handbook](https://about.gitlab.com/handbook/marketing/utm-strategy)

## :busts_in_silhouette: Team
* Lifecycle Marketing: @Aklatzkin (supported by manager, @jgragnola) 
* Growth Product:  
* Marketing Operations: 
* Data Team: 

### :bar_chart: Supporting Data
<!-- Provide any links to raw data and analysis that drove the idea for this campaign. -->
<!-- Example: analysis of namespace <> sfdc account matchup that indicated we are missing opportunities to engage -->

### :question: Open Questions
<!-- As we kickoff this campaign, what are existing open questions that we have? What do we need to address first? -->
<!-- Example: Are the users we are planning to target email compliant? Do some of these users already exist in SFDC? -->

### :link: Links
<!-- Add links like target lists, Marketo programs, etc. -->
* [Add here]()
* [Add here]()

/label ~"dg-campaigns" ~"mktg-demandgen" ~"mktg-status::wip"
```
